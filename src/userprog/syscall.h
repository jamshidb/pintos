#ifndef USERPROG_SYSCALL_H
#define USERPROG_SYSCALL_H

#include <list.h>
#include "hash.h"

typedef int mmapid_t;
#define ERROR -1

#define USER_VADDR_BOTTOM ((void *) 0x08048000)
#define STACK_HEURISTIC 32


typedef int mmapid_t;

struct file_elem {
	struct file* file; /*File*/
	size_t occurrences; /*Number of instances of file opened*/
	struct list_elem elem; /*List of file_maps in different
	 threads that have the same file opened*/
};

struct mmap_elem {
	struct file* file;
	void *start_addr;
	void *end_addr;
	struct hash_elem elem;
	mmapid_t map_id;
	struct list file_pages;
};

void syscall_init (void);
struct file_elem* is_file_open(struct file* file);
struct file_elem* file_open_(struct file* file);
struct lock filesys_lock;
void mmap_init(void);
void munmap (mmapid_t mapping);

bool check_stack_valid(const void *vaddr, void* esp);
#endif /* userprog/syscall.h */
