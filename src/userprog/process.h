#ifndef USERPROG_PROCESS_H
#define USERPROG_PROCESS_H

#include "threads/thread.h"

tid_t process_execute (const char *file_name);
int process_wait (tid_t);
void process_exit (void);
void process_activate (void);
void destroy_thread_files(struct thread *t);
bool install_page(void *upage, void *kpage, bool writable);

struct arg {
	struct list_elem elem;
	void* location;
};

#endif /* userprog/process.h */
