#include "userprog/syscall.h"
#include <stdio.h>
#include <string.h>
#include <syscall-nr.h>
#include "threads/interrupt.h"
#include "threads/thread.h"
#include "userprog/pagedir.h"
#include "threads/vaddr.h"
#include "filesys/file.h"
#include "devices/shutdown.h"
#include "userprog/process.h"
#include "filesys/filesys.h"
#include "threads/malloc.h"
#include "devices/input.h"
#include "vm/page.h"
#include "threads/pte.h"
#include "vm/page.h"
#include "threads/pte.h"
#include "vm/frame_table.h"

#define FILELIMIT 128
#define STDIN_FILENO 0
#define STDOUT_FILENO 1
#define NONRESERVED_FDS 2
#define ARG1 read_user_memory (((uint32_t *)f->esp) + 1)
#define ARG2 read_user_memory (((uint32_t *)f->esp) + 2)
#define ARG3 read_user_memory (((uint32_t *)f->esp) + 3)

static void syscall_handler(struct intr_frame *);
static void is_ptr_valid(const void* ptr);
static void check_file_ptr(const char *file);

static void syscall_handler(struct intr_frame *);
static void is_ptr_valid(const void* ptr);
static void check_file_ptr(const char *file);

static void syscall_handler(struct intr_frame *);
static void syscall_halt(void);
static void syscall_exit(int status);
static void syscall_exec(const char* cmd_line, struct intr_frame* f);
static void syscall_wait(tid_t pid, struct intr_frame* f);
static void syscall_create(const char *file, unsigned initial_size,
		struct intr_frame* f);
static void syscall_remove(const char *file, struct intr_frame* f);
static void syscall_open(const char *file, struct intr_frame* f);
static void syscall_filesize(int fd, struct intr_frame* f);
static void syscall_read(int fd, void *buffer, unsigned size,
		struct intr_frame* f);
static void syscall_write(int fd, const void *buffer, unsigned size,
		struct intr_frame* f);
static void syscall_seek(int fd, unsigned position);
static void syscall_tell(int fd, struct intr_frame* f);
static void syscall_close(int fd);
static void syscall_return(int value, struct intr_frame* f);
static mmapid_t mmap(int fd, void *addr, struct intr_frame* f);
static struct file_map* file_map_from_fd(int fd);
static uint32_t read_user_memory(void* addr);
static bool overlap(void * addr, uint32_t length);

static unsigned mmap_hash_func(const struct hash_elem *e, void *aux UNUSED);
static bool mmap_less_func(const struct hash_elem *a, const struct hash_elem *b,
		void *aux UNUSED);
static struct mmap_elem* insert_mmap_entry(struct file* file, void* addr,
		uint32_t length, mmapid_t mapid);
static void mmap_insert_page(struct mmap_elem* mmap, struct page_elem* page);
struct file *get_file(int fd);
struct list_elem *get_file_elem(int fd);
static struct list files_opened;

static int global_fd;
static mmapid_t global_map_id;

void syscall_init(void) {
	intr_register_int(0x30, 3, INTR_ON, syscall_handler, "syscall");
	lock_init(&filesys_lock);
	list_init(&files_opened);
	global_fd = NONRESERVED_FDS; /* Starting after reserved fds */
	global_map_id = 0;

}

/* Getting the file_map which has the particular fd */
static struct file_map* file_map_from_fd(int fd) {
	struct list_elem* e;
	/*loops through the file_map list which holds mappings of fd's to files
	 * and finds the file that matches the given fd*/
	for (e = list_begin(&thread_current()->file_maps);
			e != list_end(&thread_current()->file_maps); e = list_next(e)) {
		struct file_map* fm = list_entry(e, struct file_map, elem);
		if (fm->fd == fd) {
			return fm;
		}

	}
	return NULL;

}

static void syscall_handler(struct intr_frame *f) {
	int sysCall = read_user_memory(f->esp);
	switch (sysCall) {
	case SYS_HALT:
		syscall_halt();
		break;
	case SYS_EXIT:
		syscall_exit((int) ARG1);
		break;
	case SYS_EXEC:
		syscall_exec((const char*) ARG1, f);
		break;
	case SYS_WAIT:
		syscall_wait((tid_t) ARG1, f);
		break;
	case SYS_CREATE:
		syscall_create((const char*) ARG1, (unsigned) ARG2, f);
		break;
	case SYS_REMOVE:
		syscall_remove((const char*) ARG1, f);
		break;
	case SYS_OPEN:
		syscall_open((const char*) ARG1, f);
		break;
	case SYS_FILESIZE:
		syscall_filesize((int) ARG1, f);
		break;
	case SYS_READ:
		syscall_read((int) ARG1, (void *) ARG2, (unsigned) ARG3, f);
		break;
	case SYS_WRITE:
		syscall_write((int) ARG1, (const void*) ARG2, (size_t) ARG3, f);
		break;
	case SYS_SEEK:
		syscall_seek((int) ARG1, (unsigned) ARG2);
		break;
	case SYS_TELL:
		syscall_tell((int) ARG1, f);
		break;
	case SYS_CLOSE:
		syscall_close((int) ARG1);
		break;
	case SYS_MMAP:
		mmap((int) ARG1, (void *) ARG2, f);
		break;
	case SYS_MUNMAP:
		munmap((mmapid_t) ARG1);
	}
}

/* Reading from the address */
static uint32_t read_user_memory(void* addr) {
	is_ptr_valid(addr);
	return *(uint32_t*) addr;
}

/* Checking if the address is valid */
static void is_ptr_valid(const void* ptr) {
	if (ptr == NULL
			|| !is_user_vaddr(
					ptr) || pagedir_get_page (thread_current ()->pagedir, ptr) == NULL) {
		thread_exit();
	}
}

/* Check if address is page aligned */
static void is_page_aligned(void *addr) {
	if ((uint32_t) addr % PGSIZE != 0) {
		thread_exit();
	}
}

bool check_stack_valid(const void *vaddr, void* esp) {
	return (vaddr >= esp - STACK_HEURISTIC && esp > PHYS_BASE - (1 << 23));
}

/* Checking if the file address is valid */
static void check_file_ptr(const char *file) {
	is_ptr_valid(file);
	is_ptr_valid(file + strlen(file));
}

/* Returns the file which has the particular fd */
struct file *get_file(int fd) {
	struct list_elem* e;
	struct list fm = thread_current()->file_maps;
	for (e = list_begin(&fm); e != list_end(&fm); e = list_next(e)) {
		struct file_map *current_fm = list_entry(e, struct file_map, elem);

		if (current_fm->fd == fd) {
			return current_fm->file;
		}
	}
	return NULL;
}

/* Returns the list_elem which has the particular fd */
struct list_elem *get_file_elem(int fd) {
	struct list_elem* e;
	struct list fm = thread_current()->file_maps;
	for (e = list_begin(&fm); e != list_end(&fm); e = list_next(e)) {
		struct file_map *current_fm = list_entry(e, struct file_map, elem);
		if (current_fm->fd == fd) {
			return &current_fm->elem;
		}
	}
	return NULL;
}

/* Checking if fd is valid */
static void is_fd_valid(int fd) {
	if (!((fd >= NONRESERVED_FDS) && (fd < FILELIMIT) && (get_file(fd) != NULL))) {
		thread_exit();
	}
}

/* Checking if fd is valid for close */
static void is_fd_valid_for_close(int fd) {
	if (!((fd >= NONRESERVED_FDS) && (fd < FILELIMIT))) {
		thread_exit();
	}
}

/* Terminates Pintos. Shouldn't be used often  */
static void syscall_halt(void) {
	shutdown_power_off();
}

/* Returns the file which has already been opened */
struct file_elem*
file_open_(struct file* file) {
	struct list_elem* elem;
	for (elem = list_begin(&files_opened); elem != list_end(&files_opened);
			elem = list_next(elem)) {
		struct file_elem* f = list_entry(elem, struct file_elem, elem);
		if (f->file == file) {
			return f;
		}
	}
	return NULL;
}
/* Terminates the current user program */
static void syscall_exit(int status) {
	/* Sending exit status to the kernel */
	thread_current()->retvalue = status;
	thread_exit();
	return;
}

/* Runs the executable named 'cmd_line' */
static void syscall_exec(const char* cmd_line, struct intr_frame* f) {
	/* Check if the cmd_line pointer is valid */
	is_ptr_valid(cmd_line);

	lock_acquire(&filesys_lock);

	/* Returns tid of the thread that is executing */
	tid_t tid = process_execute(cmd_line);
	lock_release(&filesys_lock);
	syscall_return(tid, f);
}

/* Creates a new file called 'file' initially 'initial_size' bytes in size */
static void syscall_create(const char *file, unsigned initial_size,
		struct intr_frame* f) {
	bool is_create;
	/* Checking the file pointer if it is valid */
	check_file_ptr(file);

	/* If the file is over the range */
	if (strlen(file) > FILELIMIT) {
		/* Set is_create to false */
		is_create = 0;
	} else {
		lock_acquire(&filesys_lock);
		/* Returns true when file is created */
		is_create = filesys_create(file, initial_size);
		lock_release(&filesys_lock);
	}
	syscall_return(is_create, f);
}

/* Deletes the file */
static void syscall_remove(const char *file, struct intr_frame* f) {
	/* Checking the file pointer if it is valid */
	check_file_ptr(file);
	lock_acquire(&filesys_lock);
	/* Returns true if file removes successfully */
	bool is_remove = filesys_remove(file);
	lock_release(&filesys_lock);
	syscall_return(is_remove, f);
}

static void syscall_open(const char *file, struct intr_frame* f) {
	/* Checking if the file pointer is valid */
	check_file_ptr(file);

	lock_acquire(&filesys_lock);
	int local_fd;
	struct file* open_file = filesys_open(file);
	lock_release(&filesys_lock);
	/* if the file cannot be opened */
	if (!open_file) {
		local_fd = -1; /* returns -1 if the file cannot be opened */
	} else {
		/* Creating file_map */
		struct file_map* newFile = malloc(sizeof(struct file_map));

		/* If malloc failed */
		if (!newFile) {
			thread_exit();
		}

		/* global_fd starts at 2 which is non-reserved */
		local_fd = global_fd++;
		newFile->fd = local_fd;
		newFile->file = open_file;

		/* Creating file_elem */
		struct file_elem *file_open = file_open_(open_file);

		/* If the file has been opened previously */
		if (file_open != NULL) {
			/* Increase the occurences */
			file_open->occurrences++;
		} else {
			/* If the file is newly opened, create file_open */
			file_open = malloc(sizeof(struct file_elem));
			/* If file fails to open */
			if (!file_open) {
				thread_exit();
			}
			/* Store the file to the files_opened list */
			file_open->file = open_file;
			file_open->occurrences = 1;
			list_push_back(&files_opened, &file_open->elem);
		}

		list_push_back(&thread_current()->file_maps, &newFile->elem);
	}
	syscall_return(local_fd, f);
}

/* Returns the size in bytes of the file opened as fd */
static void syscall_filesize(int fd, struct intr_frame* f) {
	struct file* file = get_file(fd);
	lock_acquire(&filesys_lock);
	/* Returns the size of the file in bytes */
	int file_size = (int) file_length(file);
	syscall_return(file_size, f);
	lock_release(&filesys_lock);
}

static void syscall_read(int fd, void *buffer, unsigned size,
		struct intr_frame* f) {
	/* Checking if fd is valid */
	if (fd == STDOUT_FILENO || fd > FILELIMIT) {
		thread_exit();
	}
	/* Check if the buffer address is valid */
	if (!is_user_vaddr(buffer) && !is_user_vaddr((void*) buffer + size)) {
		thread_exit();
	}
	if (check_stack_valid((const void*) buffer, f->esp)) {
		grow_stack((void *) ((uint32_t) buffer & PTE_ADDR));
	}

	int result = 0;
	/* If fd is reserved for STDIN_FILENO */
	if (fd == STDIN_FILENO) {
		unsigned i = 0;
		for (i = 0; i < size; i++) {
			*(uint8_t *) buffer = input_getc();
			result++;

			buffer++;
		}
		/* Read from file */
	} else {
		/* Get file data*/
		struct file* file = get_file(fd);
		while (size > 0) {
			size_t offset = (uint32_t) buffer % PGSIZE;
			void * vaddr = (void *) ((uint32_t) buffer & PTE_ADDR);
			struct page_elem *page = get_page_entry(vaddr);
			if (page == NULL) {
				if (check_stack_valid((const void*) buffer, f->esp)) {
					grow_stack((void *) ((uint32_t) buffer & PTE_ADDR));
					page = get_page_entry(vaddr);
				}
			}
			if (!page->is_loaded) {
				page_load(page);
			}
      pin_frame(page->frame_elem->paddress);
			size_t read_bytes = offset + size > PGSIZE ? PGSIZE - offset : size;
			lock_acquire(&filesys_lock);
			result += file_read(file, buffer, read_bytes);
			lock_release(&filesys_lock);
			buffer += read_bytes;
			size -= read_bytes;
      unpin_frame(page->frame_elem->paddress);
		}
		syscall_return(result, f);
	}
}

/* Writes size bytes from buffer to the open file */
static void syscall_write(int fd, const void *buffer, unsigned size,
		struct intr_frame* f) {
	/* Checking if buffer address is valid */
	if (!is_user_vaddr(buffer) && !is_user_vaddr((void*) buffer + size)) {
		thread_exit();
	}
	if (check_stack_valid((const void*) buffer, f->esp)) {
		grow_stack((void *) ((uint32_t) buffer & PTE_ADDR));
	}
	/* Write to console */
	if (fd == STDOUT_FILENO) {
		putbuf(buffer, size);
		syscall_return(size, f);
	}

	/* write to file */
	else {
		/* Check if fd is valid */
		is_fd_valid(fd);
		/* Get file data */
		/* write size bytes from bufffer into the file	*/
		off_t result = 0;
		struct file* file = get_file(fd);
		while (size > 0) {
			size_t offset = (uint32_t) buffer % PGSIZE;
			void * vaddr = (void *) ((uint32_t) buffer & PTE_ADDR);
			struct page_elem *page = get_page_entry(vaddr);
			if (page == NULL) {
				if (check_stack_valid((const void*) buffer, f->esp)) {
					grow_stack((void *) ((uint32_t) buffer & PTE_ADDR));
					page = get_page_entry(vaddr);
				}
			}
			if (!page->is_loaded) {
				page_load(page);
			}
			pin_frame(page->frame_elem->paddress);
			size_t write_bytes =
					offset + size > PGSIZE ? PGSIZE - offset : size;
			lock_acquire(&filesys_lock);
			//        printf("reading the file \n");
			result += file_write(file, buffer, write_bytes);
			lock_release(&filesys_lock);
			//        printf("about to return the file \n");
			buffer += write_bytes;
			size -= write_bytes;
			unpin_frame(page->frame_elem->paddress);
		}
		syscall_return(result, f);

	}
}

/* Inserts the return value to the dedicated return register */
static void syscall_return(int value, struct intr_frame* f) {
	f->eax = value;
}

/* Waits for a child process pid and retrieves the exit status */
static void syscall_wait(tid_t tid, struct intr_frame* f) {
	int wait = process_wait(tid);
	syscall_return(wait, f);
}

/* Changes the next byte to be read or written in open file fd to position */
static void syscall_seek(int fd, unsigned position) {
	/* Checking if fd is valid */
	is_fd_valid(fd);
	struct file *file = get_file(fd);
	file_seek(file, position);
}

/* Returns the position of the next byte to be read or written */
static void syscall_tell(int fd, struct intr_frame* f) {
	is_fd_valid(fd);
	struct file *file = get_file(fd);
	syscall_return(file_tell(file), f);
}

/* Closes file fd */
static void syscall_close(int fd) {
	is_fd_valid_for_close(fd);
	struct file_map *filemap = file_map_from_fd(fd);
	/* if the filemap is null then we cannot retrieve the file
	 * to close */
	if (filemap == NULL) {
		return;
	}

	struct file *file = get_file(fd);
	/* if there is no file related to the fd, there is nothing
	 * to close so something's gone wrong*/
	if (file == NULL) {
		return;
	}
	/* retrieve the currently opened file's file_elem */
	struct file_elem* elem = file_open_(file);
	if (elem == NULL) {
		return;
	}
	elem->occurrences--;
	/* if occurences > 0 then the file is in use somewhere else
	 * so we won't want to destroy the file just yet*/
	if (elem->occurrences == 0) {
		lock_acquire(&filesys_lock);
		file_close(file);
		list_remove(&elem->elem);
		free(elem);
		lock_release(&filesys_lock);
	}
	list_remove(&filemap->elem);
	free(filemap);
}

static mmapid_t mmap(int fd, void *addr, struct intr_frame* f) {
	is_fd_valid(fd);
	if (addr == 0) {
		syscall_return(-1, f);
		return -1;
	}
	if ((uint32_t) addr % PGSIZE != 0) {
		syscall_return(-1, f);
		return -1;
	}
	if (fd == 0 || fd == 1) {
		syscall_return(-1, f);
		return -1;
	}
	mmapid_t mapid = global_map_id;
	global_map_id++;
	struct file* file = file_reopen(get_file(fd));
	int32_t length = file_length(file);
	if (length == 0) {
		syscall_return(-1, f);
		return -1;
	}
	if (overlap(addr, length)) {
		syscall_return(-1, f);
		return -1;
	}
	struct mmap_elem* mmap_entry = insert_mmap_entry(file, addr, length, mapid);
	uint32_t offset = 0;
	for (;;) {
		if (length - PGSIZE <= 0) {
			struct page_elem* p = insert_page_entry(addr, length,
					PGSIZE - length, offset, file, !get_deny_write(file));
			mmap_insert_page(mmap_entry, p);
			break;
		}
		struct page_elem* p = insert_page_entry(addr, PGSIZE, 0, offset, file,
				!get_deny_write(file));
		mmap_insert_page(mmap_entry, p);
		addr += PGSIZE;
		length -= PGSIZE;
		offset += PGSIZE;
	}
	syscall_return(mapid, f);
	return mapid;
}

static void mmap_insert_page(struct mmap_elem* mmap, struct page_elem* page) {
	list_push_back(&mmap->file_pages, &page->list_elem);
}

void munmap(mmapid_t mapping) {
	struct mmap_elem mmap_entry;
	mmap_entry.map_id = mapping;
	struct mmap_elem* deleted_entry = hash_entry (hash_find(&thread_current()->mmap,
			&mmap_entry.elem), struct mmap_elem, elem);
	struct list_elem *e;
	for (e = list_begin(&deleted_entry->file_pages);
			e != list_end(&deleted_entry->file_pages);) {
		struct page_elem* p = list_entry(e, struct page_elem, list_elem);
		e = list_next(e);
//		if (pagedir_is_dirty(thread_current()->pagedir, p->vaddress)) {
//			file_write_at(p->file, p->frame_elem->paddress, p->read_bytes, p->offset);
//		}
//		free_page(p);
		if (p->is_loaded) {
			page_unload(p);
		}
		free_page(p);
	}

	file_close(deleted_entry->file);
	hash_delete(&thread_current()->mmap,
			&deleted_entry->elem);
}

static struct mmap_elem* insert_mmap_entry(struct file* file, void* addr,
		uint32_t length, mmapid_t mapid) {
	struct mmap_elem* m = malloc(sizeof(struct mmap_elem));
	if (m == NULL) {
		PANIC("could not insert into memory map entry");
	}
	list_init(&m->file_pages);
	m->file = file;
	m->start_addr = addr;
	m->end_addr = addr + length;
	m->map_id = mapid;
	hash_insert(&thread_current()->mmap, &m->elem);
	return m;
}

void mmap_init(void) {
	hash_init(&thread_current()->mmap, mmap_hash_func, mmap_less_func, NULL);
}

static unsigned mmap_hash_func(const struct hash_elem *e, void *aux UNUSED) {
	struct mmap_elem* mmap = hash_entry(e, struct mmap_elem, elem);
	return (unsigned) mmap->map_id;
}

static bool mmap_less_func(const struct hash_elem *a, const struct hash_elem *b,
		void *aux UNUSED) {
	struct mmap_elem* mmap_a = hash_entry(a, struct mmap_elem, elem);
	struct mmap_elem* mmap_b = hash_entry(b, struct mmap_elem, elem);
	return mmap_a->map_id < mmap_b->map_id;
}

static bool overlap(void * addr, uint32_t length) {
	int i = 0;
	for (i = addr; i < addr + length; i += PGSIZE) {
		if (get_page_entry(addr)) {
			return true;
		}
	}
	return false;
}
