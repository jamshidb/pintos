#include "fixedpoint.h"


//Convert a integer or real number to Fixed point
//value into p.F Format by multiplying with F.
int
get_fixed_point(int x)
{
  return x * F;
}

//rounding toward zero
int get_integer(int x)
{
  return x / F;
}

//rounding to nearest
int get_integer_nearest(int x)
{
  if(x >= 0)
  {
	return (x + F / 2) / F;
  } else {
	return (x - F / 2) / F;
  }
}

int add_x_y(int x, int y)
{
  return x + y;
}

int sub_x_y(int x, int y)
{
  return x - y;
}

int add_x_n(int x, int n)
{
  return x + (n * F);
}

int sub_x_n(int x, int n)
{
  return x - (n * F);
}

int mul_x_y(int x, int y)
{
  return ((int64_t) x) * y / F;
}

int mul_x_n(int x, int n)
{
  return x * n;
}

int div_x_y(int x, int y)
{
  return ((int64_t) x) * F / y;
}

int div_x_n(int x, int n)
{
  return x / n;
}
