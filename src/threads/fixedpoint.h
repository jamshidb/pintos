#ifndef FIXED_POINT_H
#define FIXED_POINT_H

#include <stdint.h>


//p.q
#define Q 14
#define F (1 << Q)

int get_fixed_point(int x);
int get_integer(int x);
int get_integer_nearest(int x);
int add_x_y(int x, int y);
int sub_x_y(int x, int y);
int add_x_n(int x, int n);
int sub_x_n(int x, int n);
int mul_x_y(int x, int y);
int mul_x_n(int x, int n);
int div_x_y(int x, int y);
int div_x_n(int x, int n);
#endif
