#include <debug.h>
#include <inttypes.h>
#include <round.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "frame_table.h"
#include "threads/pte.h"
#include "vm/page.h"
#include "threads/synch.h"
#include "threads/malloc.h"
#include "threads/thread.h"
#include "kernel/list.h"
#include "vm/swap.h"
#include "userprog/pagedir.h"

static bool frame_less_func(const struct hash_elem *a,
    const struct hash_elem *b, void *aux);
static unsigned frame_hash_func(const struct hash_elem *e, void *aux);
static struct hash frame_table;
static struct lock frame_table_lock;
static void frame_evict();

/* initialising frame_table */
void frame_table_init() {
  hash_init(&frame_table, frame_hash_func, frame_less_func, NULL);
  lock_init(&frame_table_lock);
}

static bool frame_less_func(const struct hash_elem *a,
    const struct hash_elem *b, void *aux UNUSED) {
  struct frame_elem *frame_a = hash_entry(a, struct frame_elem, elem);
  struct frame_elem *frame_b = hash_entry(b, struct frame_elem, elem);
  return frame_a->paddress < frame_b->paddress;
}

static unsigned frame_hash_func(const struct hash_elem *e, void *aux UNUSED) {
  struct frame_elem *frame = hash_entry(e, struct frame_elem, elem);
  return (unsigned) (PTE_ADDR & (uint32_t) frame->paddress);
}

/* Obtain unused frame from the "user pool" */
void* frame_alloc(enum palloc_flags flags, struct page_elem *p) {
  lock_acquire(&frame_table_lock);
  if (flags != PAL_USER) {
    return NULL;
  }
  void *frame = palloc_get_page(flags);
  if (!frame) {
	/* evit the frame */
    frame_evict();
    frame = palloc_get_page(flags);
  }
  insert_frame_entry(frame, p);
  lock_release(&frame_table_lock);
  return frame;
}


/* evict a page from its frame in order to be able to obtain a new unused frame */
static void frame_evict() {
  struct hash_iterator i;

  hash_first(&i, &frame_table);
  struct frame_elem *f;
  while (hash_next(&i)) {
    f = hash_entry (hash_cur (&i), struct frame_elem, elem);
    /* if the frame is not pinned */
    if (!f->pinned) {
      /* check if pagedir is accessed */
      if (pagedir_is_accessed(f->page->pagedir, f->page->vaddress)) {
    	/* if it is, make set the accessed boolean to false */
        pagedir_set_accessed(f->page->pagedir, f->page->vaddress, false);
      } else {
    	/* store the page into swap */
        store_into_swap(f->page);
        /* free the frame */
        free_frame(f);
        return;
      }
    }
  }
  frame_evict();
}

/* pinning the frame */
void pin_frame(void *paddress){
  struct frame_elem *frame = get_frame_entry(paddress);
  frame->pinned = true;
}

/* unpinning the frame */
void unpin_frame(void *paddress){
  struct frame_elem *frame = get_frame_entry(paddress);
  frame->pinned = false;
}

/* insert a frame to frame_table */
struct frame_elem* insert_frame_entry(void *paddress, struct page_elem *p) {
  struct frame_elem* new_frame = malloc(sizeof(struct frame_elem));
  if (new_frame == NULL) {
    PANIC("Could not allocate new_frame");
  }
  new_frame->pinned = true;
  new_frame->paddress = paddress;
  new_frame->page = p;
  hash_insert(&frame_table, &new_frame->elem);
  return new_frame;
}

/* getting the frame from hash table */
struct frame_elem* get_frame_entry(void *paddress) {
  struct frame_elem frame;
  frame.paddress = paddress;
  lock_acquire(&frame_table_lock);
  struct hash_elem *h = hash_find(&frame_table, &frame.elem);
  lock_release(&frame_table_lock);
  if (h == NULL) {
    return NULL;
  }
  return hash_entry(h, struct frame_elem, elem);
}

/* freeing the frame */
void free_frame(struct frame_elem *frame) {
  palloc_free_page(frame->paddress);
  /* delete from the frame table */
  hash_delete(&frame_table, &frame->elem);
  free(frame);
}

