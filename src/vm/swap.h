/*
 * swap.h
 *
 *  Created on: 19 Mar 2015
 *      Author: hal213
 */

#ifndef SWAP_H_
#define SWAP_H_

#include "devices/block.h"
#include <bitmap.h>
#include "threads/vaddr.h"
#include "threads/synch.h"
#include "vm/page.h"
#include "vm/frame_table.h"

#define BLOCKS_PER_PAGE 8
#define BITS_PER_PAGE 1
#define SWAP_FREE 0
#define SWAP_IN_USE 1

struct lock swap_lock; 		/* lock for the swap map */
struct block* swap_block;	/* device which the swap pages are read/written */
struct bitmap* swap_map; 	/* bitmap */

void swap_init(void);
void store_into_swap(struct page_elem *page);
void load_from_swap(struct page_elem *page);

#endif /* SWAP_H_ */
