#include "hash.h"
#include "threads/palloc.h"
#include "vm/page.h"

#ifndef VM_FRAME_TABLE_H
#define VM_FRAME_TABLE_H


struct frame_elem {
	struct hash_elem elem;	/* hash_elem of the frame_elem */
	void *paddress;			/* physical address */
	struct page_elem *page; /* pointer to page */
	bool pinned;			/* boolean to check whether it is pinned */
};


struct frame_elem* get_frame_entry(void *paddress);
struct frame_elem* insert_frame_entry(void *paddress, struct page_elem *p);
void* frame_alloc(enum palloc_flags flags, struct page_elem *p) ;
void frame_table_init(void);
void free_frame(struct frame_elem *frame);
void unpin_frame(void *paddress);
void pin_frame(void *paddress);

#endif
