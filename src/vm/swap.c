#include "swap.h"

/* initialise swap map */
void swap_init()
{
	swap_block = block_get_role(BLOCK_SWAP);
	lock_init(&swap_lock);
	block_sector_t bsize = block_size(swap_block);
	swap_map = bitmap_create(bsize / BLOCKS_PER_PAGE);
}

/* store the page into swap map */
void store_into_swap(struct page_elem *page) {
	ASSERT(swap_block);
	ASSERT(swap_map);

	lock_acquire(&swap_lock);
	size_t free_index
			= bitmap_scan_and_flip(swap_map, 0, BITS_PER_PAGE, SWAP_FREE);

	ASSERT(free_index != BITMAP_ERROR);
		size_t i = 0;
	  for (i = 0; i < BLOCKS_PER_PAGE; i++)
	    {
	      block_write(swap_block, free_index * BLOCKS_PER_PAGE + i,
			  (uint8_t *) page->frame_elem->paddress + i * BLOCK_SECTOR_SIZE);
	    }
	page->swap_index = free_index;
	page->type = SWAP;
	lock_release(&swap_lock);
}

/* load the page from swap map */
void load_from_swap(struct page_elem *page) {
	ASSERT(swap_block);
	ASSERT(swap_map);

	size_t index = page->swap_index;
	lock_acquire(&swap_lock);

	if (bitmap_test(swap_map, index) == SWAP_FREE)
	    {
	      PANIC ("Trying to swap in a free block! Kernel panicking.");
	    }
	bitmap_flip(swap_map, index);
	size_t i = 0;
	for (i = 0; i < BLOCKS_PER_PAGE; i++) {
		block_read(swap_block, index*BLOCKS_PER_PAGE + i,
				(uint8_t) page->frame_elem->paddress + i * BLOCK_SECTOR_SIZE);
	}
	page->type = FILE;
	lock_release(&swap_lock);
}
