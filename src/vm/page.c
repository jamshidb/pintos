#include "vm/page.h"
#include "vm/frame_table.h"
#include <debug.h>
#include <inttypes.h>
#include <round.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "threads/malloc.h"
#include "threads/synch.h"
#include "threads/vaddr.h"
#include "threads/interrupt.h"
#include "userprog/syscall.h"
#include "userprog/pagedir.h"
#include "filesys/file.h"
#include "threads/thread.h"
#include "userprog/process.h"
#include "vm/swap.h"
static struct lock page_table_lock;

static bool page_less_func(const struct hash_elem *a, const struct hash_elem *b,
    void *aux UNUSED);
static unsigned page_hash_func(const struct hash_elem *e, void *aux UNUSED);

/* initialising page table */
void page_table_init(struct hash *page_table) {
  lock_init(&page_table_lock);
  hash_init(page_table, page_hash_func, page_less_func, NULL);
}

static bool page_less_func(const struct hash_elem *a, const struct hash_elem *b,
    void *aux UNUSED) {
  struct page_elem *page_a = hash_entry(a, struct page_elem, elem);
  struct page_elem *page_b = hash_entry(b, struct page_elem, elem);
  return page_a->vaddress < page_b->vaddress;
}

static unsigned page_hash_func(const struct hash_elem *e, void *aux UNUSED) {
  struct page_elem *page = hash_entry(e, struct page_elem, elem);
  return hash_int((int) (page->vaddress));
}

/* insert page into page_table */
struct page_elem* insert_page_entry(void *vaddress, uint32_t read_bytes,
    uint32_t zero_bytes, int32_t ofs, struct file *file, bool writable) {
  struct page_elem* new_page = malloc(sizeof(struct page_elem));
  if (new_page == NULL) {
    PANIC("Could not allocate new_page");
  }
  	/* set */
	new_page->pagedir = thread_current()->pagedir;
	new_page->file = file;
	new_page->offset = ofs;
	new_page->type = FILE;
	new_page->read_bytes = read_bytes;
	new_page->zero_bytes = zero_bytes;
	new_page->writable = writable;
	new_page->vaddress = vaddress;
	lock_acquire(&page_table_lock);
	hash_insert(&thread_current()->page_table, &new_page->elem);
	lock_release(&page_table_lock);
	return new_page;
}

/* get page */
struct page_elem* get_page_entry(void *vaddress) {
  struct page_elem page;

  page.vaddress = pg_round_down(vaddress);
  struct hash_elem *h = hash_find(&thread_current()->page_table, &page.elem);
  if (h == NULL) {
    return NULL;
  }
  return hash_entry(h, struct page_elem, elem);
}


/* load page */
bool page_load(struct page_elem *page) {
  struct frame_elem* frame = insert_frame_entry(frame_alloc(PAL_USER, page),
      page);
  page->frame_elem = frame;
  frame->page = page;
  if (page->read_bytes > 0) {
    lock_acquire(&filesys_lock);
    if ((int) page->read_bytes
        != file_read_at(page->file, frame->paddress, page->read_bytes,
            page->offset)) {
      lock_release(&filesys_lock);
			return false;
		}
		lock_release(&filesys_lock);
		memset(frame->paddress + page->read_bytes, 0, page->zero_bytes);
	} else if (page->type==SWAP) {
	  load_from_swap(page);
	} else {
	  memset(frame->paddress, 0, PGSIZE);
	}

	if (!pagedir_set_page(thread_current()->pagedir, page->vaddress,
			frame->paddress, page->writable)) {
    return false;
  }
  page->is_loaded = true;
  unpin_frame(frame->paddress);
  return true;
}

bool grow_stack(void *vaddress) {
  struct page_elem *new_page = insert_page_entry(vaddress, 0, PGSIZE, 0, NULL,
      true);
  if (new_page == NULL) {
    return false;
  }
  return true;
}


/* unload page */
void page_unload(struct page_elem* page){
	/* check if it is a type file, if it is dirty, writable and exists */
  if (page->type == FILE
      && pagedir_is_dirty(page->pagedir, page->vaddress )
      && page->writable
      && page->file != NULL){
    file_seek(page->file, page->offset);
    file_write(page->file, page->frame_elem->paddress, page->offset);
  }
  /* clear the page */
  pagedir_clear_page(page->pagedir, page->vaddress);
  page->frame_elem = NULL;
  page->is_loaded = false;

}

/* free page */
void free_page(struct page_elem* page){
  lock_acquire(&page_table_lock);
  /* delete page from the page table */
  hash_delete(&thread_current()->page_table, &page->elem);
  pagedir_clear_page(page->pagedir, page->vaddress);
  free(page);
  lock_release(&page_table_lock);
}
