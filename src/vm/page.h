#ifndef PAGE_H_
#define PAGE_H_

#include "hash.h"

enum type {
	FILE, SWAP
};

struct page_elem {

	void *vaddress;					/* virtual address */
	struct frame_elem *frame_elem;	/* pointer to frame element */
	struct hash_elem elem;		    /* hash_elem of page */
	uint32_t* pagedir;				/* page directory */
	struct list_elem list_elem;

	enum type type;					/* type of a file */
	bool writable;					/* boolean to check if it is writable */
	bool is_loaded;					/* boolean to check if it is loaded */

	/* For file */
	struct file *file;				/* pointer to a file */
	size_t offset;					/* offset from which the page was read */
	size_t read_bytes;				/* valid read bytes */
	size_t zero_bytes;				/* zero bytes */

	/* For swap */
	size_t swap_index;
};

void page_table_init(struct hash *page_elem);
struct page_elem* insert_page_entry(void *vaddress, uint32_t read_bytes,
		uint32_t zero_bytes, int32_t ofs, struct file *file, bool writable);
void page_tabe_destroy(void);
bool page_load(struct page_elem *page);
struct page_elem* get_page_entry(void *vaddress);
bool grow_stack(void *vaddress);
void page_unload(struct page_elem *page);
void free_page(struct page_elem *page);
#endif /* PAGE_H_ */
